package com.epam.decorator;

import com.epam.decorator.elements.Button;
import com.epam.decorator.elements.InputField;
import com.epam.decorator.elements.PageElement;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.Field;

public class Decorator extends DefaultFieldDecorator {

  public Decorator(ElementLocatorFactory factory) {
    super(factory);
  }

  @Override
  public Object decorate(ClassLoader loader, Field field) {

    ElementLocator locator = factory.createLocator(field);

    if (Button.class.isAssignableFrom(field.getType())) {
      final Button button = new Button(proxyForLocator(loader, locator));
      return button;
    } else if (InputField.class.isAssignableFrom(field.getType())) {
      final InputField textInput = new InputField(proxyForLocator(loader, locator));
      return textInput;
    } else if (PageElement.class.isAssignableFrom(field.getType())) {
      final PageElement pageElement = new PageElement(proxyForLocator(loader, locator));
      return pageElement;
    } else {
      return super.decorate(loader, field);
    }
  }
}
