package com.epam.decorator.elements;

import org.openqa.selenium.WebElement;

public class InputField extends PageElement {

  public InputField(WebElement webElement) {
    super(webElement);
  }

  @Override
  public void click() {
    webElement.click();
  }

  @Override
  public void sendKeys(CharSequence... charSequences) {
    webElement.sendKeys(charSequences);
  }
}
