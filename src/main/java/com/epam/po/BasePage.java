package com.epam.po;

import com.epam.decorator.Decorator;
import com.epam.utils.DriverManager;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class BasePage {

  public BasePage() {
    PageFactory.initElements(new Decorator(new DefaultElementLocatorFactory(DriverManager.getWebDriver())),this);
  }
}
