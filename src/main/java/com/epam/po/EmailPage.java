package com.epam.po;

import com.epam.decorator.elements.Button;
import com.epam.decorator.elements.InputField;
import com.github.javafaker.Faker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static com.epam.utils.DriverManager.getWebDriver;

public class EmailPage extends BasePage {

  private static final Logger logger = LogManager.getLogger(EmailPage.class);
  private WebDriverWait wait = new WebDriverWait(getWebDriver(), 70);
  private Faker fake = new Faker();
  private String numberOfEmails;

  @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
  private Button composeButton;

  @FindBy(xpath = "(//textarea[@class='vO'])[1]")
  private InputField recipientEmailInputField;

  @FindBy(xpath = "//div[@class='aoD az6']//input")
  private InputField subjectNameInputField;

  @FindBy(xpath = "//div[@class='Am Al editable LW-avf tS-tW']")
  private InputField textAreaInputField;

  @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
  private Button sendButton;

  @FindBy(className = "Ha")
  private Button closeButton;

  public EmailPage() {

  }

  public EmailPage clickByComposeButton() {
    logger.info("Click by compose button");
    numberOfEmails = getNumberOfEmailsBeforeSending();
    composeButton.click();
    return this;
  }

  public EmailPage clickBySendButton() {
    logger.info("Click by send button");
    sendButton.click();
    return this;
  }

  public EmailPage fillRecipientEmailInputField() {
    logger.info("Fill recipient address");
    recipientEmailInputField.sendKeys("volodya2127@gmail.com");
    recipientEmailInputField.sendKeys(Keys.ENTER);
    return this;
  }

  public EmailPage fillSubjectNameInputField() {
    logger.info("Fill subject field");
    subjectNameInputField.sendKeys(fake.internet().domainName());
    return this;
  }

  public EmailPage fillTextAreaInputField() {
    logger.info("Fill text area");
    textAreaInputField.click();
    textAreaInputField.sendKeys(fake.internet().macAddress());
    return this;
  }

  public EmailPage clickByCloseButton() {
    logger.info("Click by close button");
    closeButton.click();
    return this;
  }

  public EmailPage verifyIsMessageInDraftFolder() {
    int numberOne;
    int numberTwo = Integer.valueOf(numberOfEmails);
    logger.info("Verify is message in draft folder");
    numberOne = Integer.valueOf(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
            "//span[@class='nU n1']//a[contains(text(),'Чернетки')]/ancestor::div[@class='aio UKr6le']//div"))).getText());
    Assert.assertEquals(compareDigits(numberTwo, numberOne), 1);
    return this;
  }

  private int compareDigits(Integer numberOne, Integer numberTwo) {
    if (numberTwo > numberOne) {
      return 1;
    }
    if (numberTwo < numberOne) {
      return -1;
    }
    return 0;
  }

  public EmailPage clickByDraftMessage() {
    logger.info("Click by draft message");
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
            "//span[@class='nU n1']//a[contains(text(),'Чернетки')]"))).click();
    Actions actions = new Actions(getWebDriver());
    actions.moveToElement(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
            "//div[contains(@class,'yW')]//span[contains(text(),'Чернетка')]")))).click().build().perform();
    return this;
  }

  private String getNumberOfEmailsBeforeSending() {
    return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
            "//span[@class='nU n1']//a[contains(text(),'Чернетки')]/ancestor::div[@class='aio UKr6le']//div"))).getText();
  }
}
