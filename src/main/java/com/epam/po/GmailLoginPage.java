package com.epam.po;

import com.epam.decorator.elements.Button;
import com.epam.decorator.elements.InputField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.FindBy;


public class GmailLoginPage extends BasePage {

  private static final Logger logger = LogManager.getLogger(GmailLoginPage.class);

  @FindBy(xpath = "//input[@type='email']")
  private InputField emailInputField;

  @FindBy(xpath = "//div[@class='Xb9hP']//input[@type='password']")
  private InputField passwordInputField;

  @FindBy(id = "identifierNext")
  private Button emailNextButton;

  @FindBy(id = "passwordNext")
  private Button passwordNextButton;

  public GmailLoginPage() {

  }

  public GmailLoginPage fillEmailInputField(String email) {
    logger.info("Fill Email input field");
    emailInputField.sendKeys(email);
    return this;
  }

  public GmailLoginPage fillPasswordInputField(String password) {
    logger.info("Fill Password input field");
    passwordInputField.sendKeys(password);
    return this;
  }

  public GmailLoginPage clickByEmailNextButton() {
    logger.info("Click by next button");
    emailNextButton.click();
    return this;
  }

  public GmailLoginPage clickByPasswordNextButton() {
    logger.info("Click by next button");
    passwordNextButton.click();
    return this;
  }
}
