package com.epam.po.bo;

import com.epam.po.EmailPage;

public class EmailBO {
  private EmailPage emailPage;

  public EmailBO() {
    emailPage = new EmailPage();
  }

  public EmailBO writeEmail() {
    emailPage
            .clickByComposeButton()
            .fillRecipientEmailInputField()
            .fillSubjectNameInputField()
            .fillTextAreaInputField()
            .clickByCloseButton();
    return this;
  }

  public EmailBO verifyThatMessageSavedInDraftFolder() {
    emailPage.verifyIsMessageInDraftFolder();
    return this;
  }

  public EmailBO sendMessage() {
    emailPage
            .clickByDraftMessage()
            .clickBySendButton();
    return this;
  }
}
