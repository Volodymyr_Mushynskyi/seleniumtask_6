package com.epam.po.bo;

import com.epam.po.GmailLoginPage;

public class GmailLoginBO {
  private GmailLoginPage gmailLoginPage;

  public GmailLoginBO() {
    gmailLoginPage = new GmailLoginPage();
  }

  public GmailLoginBO login(String email, String password) {
    gmailLoginPage
            .fillEmailInputField(email)
            .clickByEmailNextButton()
            .fillPasswordInputField(password)
            .clickByPasswordNextButton();
    return this;
  }
}
