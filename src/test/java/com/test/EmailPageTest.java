package com.test;

import com.epam.po.bo.EmailBO;
import com.epam.po.bo.GmailLoginBO;
import com.epam.utils.ProjectProperties;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

public class EmailPageTest extends BaseTest {

  @DataProvider(parallel = true)
  public Iterator<Object[]> users() {
    ProjectProperties properties = new ProjectProperties();
    return Stream.of(
            new Object[]{properties.getProperty("first_email"), properties.getProperty("password")},
            new Object[]{properties.getProperty("second_email"), properties.getProperty("password")},
            new Object[]{properties.getProperty("third_email"), properties.getProperty("password")}).iterator();
  }

  @Test(dataProvider = "users")
  public void sendingEmail(String email, String password) {
    new GmailLoginBO()
            .login(email, password);
    new EmailBO()
            .writeEmail()
            .verifyThatMessageSavedInDraftFolder()
            .sendMessage();
  }
}
